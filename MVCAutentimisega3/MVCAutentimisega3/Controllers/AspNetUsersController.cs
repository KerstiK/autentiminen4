﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega3.Models;

namespace MVCAutentimisega3.Models
{
    partial class AspNetUser
    {
        public string FullName => $"{FirstName} {LaststName}";
    }
}

namespace MVCAutentimisega3.Controllers
{
    public class AspNetUsersController : Controller
    {
        private ParemEsimeneEntities db = new ParemEsimeneEntities();

        // GET: AspNetUsers
        public ActionResult Index() 

        {
            return View(db.AspNetUsers.ToList());
        }
        
        // Kui ma tahan näidata seda lehte ainult autoriseeritud kasutajale!
        //{
        //    if (Request.IsAuthenticated)
        //    {
        //        AspNetUser user = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).FirstOrDefault(); // Kas meil on selle nimega inimene või mitte sisse logitud
        //        if (user != null) // me näitame nime ainult registreeritud kasutajale
        //        {
        //            String fullname = user?.FullName ?? "tundmatu"; // küsimärk, et kui on vastuseks null, ?? kui avaldis "null" siis kirjuta see sõna
        //            ViewBag.FullName = fullname; // ViewBag on seljakott, kuhu antkase View'le asju kaasa, mida ta saab kasutada seal kus vaja. Sellega muudetakse see info View'le nähtavaks

        //            //if (user.Is...) // oma piltide ja albumite kuvamiseks!!!! Admin näeb kõike!
        //            //{
        //            //    var user = db.
        //            //    return View(db.AspNetUsers.ToList()); // kui on andmin, siis näitame seda nimekirja
        //            //}
        //            //else
        //            //{
        //            //    return View(db.AspNetUsers.ToList()); // siia on vaja siis panna, et mida näidatakse, kui ei ole admin
        //            //}
        //        }
        //    }
         
        //        return RedirectToAction("Index", "Home"); // sellega me saadame ta teisele lehele
        //    }
        //}

        // GET: AspNetUsers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AspNetUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LaststName,BirthDate,Hometown")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.AspNetUsers.Add(aspNetUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aspNetUser);
        }

        // GET: AspNetUsers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,FirstName,LaststName,BirthDate,Hometown")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aspNetUser);
        }

        // GET: AspNetUsers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: AspNetUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
