﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega3.Models;
using System.IO; // lisan siia!!!! 

namespace MVCAutentimisega3.Controllers
{
    public class DataFilesController : Controller
    {
        private ParemEsimeneEntities db = new ParemEsimeneEntities();

        //Siia tuleb teha outputcache - vt Ennu failist!!!!  siis nad jäetakse minu masinasse mingiks ajaks meelde
        public ActionResult Content(int id) // andmebaasist faili lugemine
        {
           DataFile file= db.DataFiles.Find(id); // otsib andmebaasist objekti ainult ühenda (mitte kollektsioonina, kui seda ei ole, siis väärtus null
            if (file == null) return HttpNotFound();
            byte[] buff = file.Content; // loen andmed välja 
            return File(buff, file.ContentType); // saadan vastuseks
        }

        // GET: DataFiles
        public ActionResult Index()
        {
            return View(db.DataFiles.ToList());
        }

        // GET: DataFiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // GET: DataFiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DataFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ContentType")] DataFile dataFile, HttpPostedFileBase file) // kustutan siit contenti ära ja lisan siia http..
            // Nb siin selle http... nimi file peab olema sama, mis on creat views sellele väljale antud nimi 
        {
            // TODO: LOEME FAILI SISSE
                    if (file == null) return View(dataFile); // saan kätte faili
                if (file.ContentLength == 0) return View(dataFile); // me ei salvesta kui on tühi fail, kontrollin, et fail ikka loeti sisse
            using (BinaryReader reader = new BinaryReader(file.InputStream))
                // Lisame siia using bloki, et teeme seda ainult selle korra ja siis viskame mälust välja selle bloki käigus
            {
                ; // see loeb http-ga saadetud faili sisu lugeda
                byte[] buff = reader.ReadBytes(file.ContentLength); // loeme nad arrayssee buff. Loe nii palju, kui selle length o


                //if (ModelState.IsValid)
                {
                    dataFile = new DataFile
                    {
                        ContentType = file.ContentType,
                        Name = file.FileName.Split('\\').Last(), // võta failinimest vaid viimase osa peale \
                        Content = buff,
                    };

                    db.DataFiles.Add(dataFile);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            
        }

        // GET: DataFiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // POST: DataFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ContentType,Content")] DataFile dataFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dataFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dataFile);
        }

        // GET: DataFiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // POST: DataFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DataFile dataFile = db.DataFiles.Find(id);
            db.DataFiles.Remove(dataFile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
