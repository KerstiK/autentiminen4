﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega3.Models;
using System.IO; //LISADA

namespace MVCAutentimisega3.Controllers
{
    public class PicturesController : Controller
    {
        private ParemEsimeneEntities db = new ParemEsimeneEntities();

        // GET: Pictures
        public ActionResult Index()
        {
            var pictures = db.Pictures.Include(p => p.AspNetUser).Include(p => p.DataFile).Include(p => p.Album).Include(p => p.Category);
            return View(pictures.ToList());
        }

        // GET: Pictures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        // GET: Pictures/Create
        public ActionResult Create()
        
        {
            if (!Request.IsAuthenticated) return RedirectToAction("Index");
            AspNetUser user = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).Single();

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Fullaame", user.Id); 


            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email");
            //ViewBag.FileId = new SelectList(db.DataFiles, "Id", "Name"); // ei ole vaja
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name");
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name");
            return View(new Picture { UserId = user.Id, Created = DateTime.Now });
        }

        // POST: Pictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,Name,Description,Created,Visibility,AlbumId,CategoryId")] Picture picture, HttpPostedFileBase file)
            // Siin lisasime, et tuleb fail ka
            // siit võtsime fileId ära
        {
            if (ModelState.IsValid)
            {
                if (ModelState.IsValid && file  != null && file.ContentLength>0)
                // Ma hakkan pilti salvestama ainult siis kui kõik väljad on täidetud, fail on olemas ja faili pikkus on suurem kui null. 

                using (BinaryReader reader = new BinaryReader(file.InputStream))
                // Lisame siia using bloki, et teeme seda ainult selle korra ja siis viskame mälust välja selle bloki käigus
                {
                    ; // see loeb http-ga saadetud faili sisu lugeda
                    byte[] buff = reader.ReadBytes(file.ContentLength); // loeme nad arrayssee buff. Loe nii palju, kui selle length o


                    //if (ModelState.IsValid)
                    {
                        DataFile dataFile = new DataFile
                        {
                            ContentType = file.ContentType,
                            Name = file.FileName.Split('\\').Last(), // võta failinimest vaid viimase osa peale \
                            Content = buff,
                        };

                        db.DataFiles.Add(dataFile);
                        db.SaveChanges();
                        // Sellel hetkel tekib DataFile Id ja ta pannakse DataFile tabelisse

                        picture.FileId = dataFile.Id;
                        db.Pictures.Add(picture);
                        db.SaveChanges(); 

                        return RedirectToAction("Index");
                    }
                }
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", picture.UserId);
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "Name", picture.FileId);
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "UserId", picture.AlbumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", picture.CategoryId);
            return View(picture);
        }

        // GET: Pictures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", picture.UserId);
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "Name", picture.FileId);
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "UserId", picture.AlbumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", picture.CategoryId);
            return View(picture);
        }

        // POST: Pictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,UserId,FileId,Name,Description,Created,Visibility,AlbumId,CategoryId")] Picture picture)
        {
            if (ModelState.IsValid)
            {
                db.Entry(picture).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", picture.UserId);
            ViewBag.FileId = new SelectList(db.DataFiles, "Id", "Name", picture.FileId);
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "UserId", picture.AlbumId);
            ViewBag.CategoryId = new SelectList(db.Categories, "Id", "Name", picture.CategoryId);
            return View(picture);
        }

        // GET: Pictures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Picture picture = db.Pictures.Find(id);
            if (picture == null)
            {
                return HttpNotFound();
            }
            return View(picture);
        }

        // POST: Pictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Picture picture = db.Pictures.Find(id);
            db.Pictures.Remove(picture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
