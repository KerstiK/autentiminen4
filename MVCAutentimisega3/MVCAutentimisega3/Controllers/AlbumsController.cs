﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisega3.Models;

namespace MVCAutentimisega3.Controllers
{
    public class AlbumsController : Controller
    {
        private ParemEsimeneEntities db = new ParemEsimeneEntities();

        // GET: Albums
        public ActionResult Index()
        {
            var albums = db.Albums.Include(a => a.AspNetUser);
            return View(albums.ToList());
        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // GET: Albums/Create 
        // siia ette panna authorized - siis saab ainult registreeritud inimene siia
        // TODO:siia lisasime userituvastamise
        // praegu antakse ikkagi list ette - view's muuda ära
        public ActionResult Create()
        {
            // Kas meil on see inimene
            if (!Request.IsAuthenticated) return RedirectToAction("Index"); // kui kasutajat ei tunta, sisi suunatakse ta avalehele tagasi
            AspNetUser user = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).Single(); // Single lõppu - peab omea 1, muidu kollektsiooni. 
                                                                                                 // võtame AspNetUserite hulgas selle nime, kes seal on ning võtame selle ühe.
            // Täidame ära vormi andmetega
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FullName", user.Id); // ta teeb siia dropdowni
            // Siia siis ütleme, et vormile lähevad anmded kaasa

            return View(new Album { UserId = user.Id });
            // Kui view on tühi saadab tühja välja aga meil on userID eeltäidetud. Me anname vormile ette täidetud väljad
            // view saadab täitmiseks tühja uue albumi, kus on juba üks väärtus vaikimisi 
        }

        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,OrderId,UserId,Name,Description,Visibility,CoverPictureId")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Albums.Add(album);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "FullName", album.UserId);
            return View(album);
        }

        // GET: Albums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", album.UserId);
            return View(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,OrderId,UserId,Name,Description,Visibility,CoverPictureId")] Album album)
        {
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.AspNetUsers, "Id", "Email", album.UserId);
            return View(album);
        }

        // GET: Albums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
